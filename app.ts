﻿import * as express from 'express';
import * as path from 'path';
const morgan: any = require('morgan');
// let cors = require('cors');  // https://www.npmjs.com/package/cors
// let validator = require('validator');  // form validation middleware, for future use
import { connection } from 'mongoose';
import { dbConn } from './models/dbModel';
// note:  some of the models also require mongoose and other middlewares already used here.
// however, requiring them in those locations as well does not lead to a performance loss.
// 'When require()-ing a module in Node.js the module is only loaded once and then cached in require.cache.'
// 'When you then require() the module a second or third time you will get the exact same instance of the
// module that was loaded the first time'
// see:  https://www.reddit.com/r/webdev/comments/68m632/should_i_require_mongoose_on_every_model_file_i/  '
// const vs var when using require...  https://stackoverflow.com/questions/23483926/const-vs-var-while-requiring-a-module
// typescript - let vs. var... https://basarat.gitbooks.io/typescript/content/docs/let.html

// set development so we can see stack trace of errors
// var env = process.env.NODE_ENV || 'development';
// import routes
import routes from './routes/public';  // unprotected/public routes
import users from './routes/user';  // user control routes (login, logout, etc.)
import authRoutes from './routes/auth';  // protected routes (must be logged-in)

// declare express app
let app: any = express();

const start: any = function (): void {
    // view engine setup
    app.set('views', path.join(__dirname, 'views'));  // tells app where our views are kept
    app.set('view engine', 'pug');  // sets our view engine to PUG
    app.use(express.static(path.join(__dirname, 'public')));  // path to static files

    // setup cors
    // app.use(cors());  // i'm not using this aat the moment

    // use routes
    app.use('/', routes);
    app.use('/users', users);
    app.use('/auth', authRoutes);

    // setup logging
    app.use(morgan('dev'));  // log every request to the console

    // catch 404 and forward to error handler
    app.use(function (req: express.Request, res: express.Response, next: Function): any {
        var err: any = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // general error handlers
    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use((err: any, req: express.Request, res: express.Response, next: Function) => {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use((err: any, req: express.Request, res: express.Response, next: Function) => {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
};

// establish database connection
// you open connection once and reuse...see:
// https://stackoverflow.com/questions/14495975/why-is-it-recommended-not-to-close-a-mongodb-connection-anywhere-in-node-js-code
dbConn(console.log('dbConn initiated!'));

// once db connection is established
connection.once('open', function (): void {
    console.log('users DB is open');
    // start the express app
    start();
});

// if db can't connect
connection.on('error', function (err: any): void {
    console.error('failed to connect to db - ', err);
});

// if db disconnects
connection.on('disconnected', function (): void {
    console.log('mongoose db connection disconnected!');
});

// function for closing connection if app terminates
const closeConn: any = function (): void {
    connection.close(function (): void {
        console.log('mongoose db connection closed because app closed!');
        process.exit(0);
    });
};

// if app closes or terminates
process.on('SIGINT', closeConn).on('SIGTERM', closeConn);

module.exports = app;
