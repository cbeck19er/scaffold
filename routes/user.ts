﻿/*
 *  USERS ROUTING
 */
import { Request, Response, Router } from 'express';
let bodyParser: any = require('body-parser');
let router: any = Router();
import { passport, logout, login, create } from '../controllers/user';

// define middlewares this router will use
let userMiddlewares: Array<any> = [
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    passport.initialize()
];
router.use(userMiddlewares);

// USER routes
router.get('/logout', logout);  // GET logout, logout user
router.post('/login', login);  // POST an existing user, login
// fyi: this is an example of how to inject a middleware into a single get request
// router.post('/users/create', bodyParser.json(), UserCtrl.create);
router.post('/create', create);  // POST a new user, create

// GET create user page
router.get('/create', (req: Request, res: Response) => {
    res.render('create', {
        title: 'Create a User', username: 'sampleuser'
    });
});

// GET login page
router.get('/login', (req: Request, res: Response) => {
    res.render('login', {
        title: 'Login'
    });
});

export default router;
