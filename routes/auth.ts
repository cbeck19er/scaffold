﻿/*
 *  PROTECTED ROUTING
 */
import { Router } from 'express';
let authRouter: any = Router();  // todo:  fix implicit any's
let cookieParser: any = require('cookie-parser');
let Auth: any = require('../middlewares/auth');
let authorize: any = Auth.authorize;
import { loggedIn } from '../controllers/user';

// define middlewares this router will use
let authMiddlewares: Array<any> = [
    cookieParser(),
    authorize
];
authRouter.use(authMiddlewares);

// protected routes
authRouter.get('/:reqUser', loggedIn);

export default authRouter;
