﻿/*
 * Public Routes
 */
import { Request, Response, Router } from 'express';
let router: any = Router();

router.get('/', (req: Request, res: Response) => {
    res.render('index', { title: 'Protected Routes Scaffold' });
});

export default router;
