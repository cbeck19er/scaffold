﻿/*
 * Error routes
 */
import { Router, Request, Response } from 'express';
let router: any = Router();

router.get('/', (req: Request, res: Response) => {
    res.render('error', { title: 'Error' });
});

export default router;
