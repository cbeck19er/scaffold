﻿# Cloud - a scaffolding for protected routes
    MongoDB, Express, Node, Pug project in TypeScript 2
    Rest API for user creation and login
    User authentication and encryption

--------
## Uses

This can be a jumping-off point for a variety of projects which require user-authentication.  
An API for a service, a web-app, a cutom CMS, etc.

--------
### Notes

-I've been converting the project to TypeScript, please excuse implicit 'any' where they may exist 
as I work to give proper typedefs where required.  This is my first go at TypeScript :)  
-I'll be slowly adding to it and making fixes in my spare time.  
-I've adopted the TypeScript guideline on NOT starting interface names with a capital 'I'  
--see: https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines

--------
### Todo

-finish adding proper typedefs  
-add unit testing  
-add auth routes/views for editing and deleting user  
-add method for resending confirmation email if first attempt fails  
