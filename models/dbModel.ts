import { connect } from 'mongoose';

// see https://github.com/Automattic/mongoose/issues/5399 for info on the connect options below!
interface OptionsInterface {
    useMongoClient: boolean;
    autoIndex: boolean;
    reconnectTries: number;
    reconnectInterval: number;
    poolSize: number;
    bufferMaxEntries: number;
};

const options: OptionsInterface = {
    useMongoClient: false,
    autoIndex: true, // set to 'true' for development
    reconnectTries: Number.MAX_VALUE, // never stop trying to reconnect
    reconnectInterval: 500, // reconnect every 500ms
    poolSize: 10, // maintain up to 10 socket connections
    // if not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
};

export let dbConn: any = function (): void {
    const dbLoc: string = ('your database location');  // database location
    console.log('dbLoc from db model = ', dbLoc);
    connect(dbLoc, options);
    console.log('database connection attempt initiated');
};
