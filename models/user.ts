import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
let salt: string = bcrypt.genSaltSync(10);

export interface UserWithPassInterface {
    email: string;
    username: string;
    password: string;
    admin: boolean;
    created: Date;
    updated: Date;
}

export interface UserNoPassInterface {
    email: string;
    username: string;
    admin: boolean;
    created: Date;
    updated: Date;
}

export interface UserMethodsInterface extends UserWithPassInterface {
    auth(inputPassword: string, cb: any): any;
    withoutPassword(u: UserWithPassInterface): UserNoPassInterface;
}

export class User implements UserWithPassInterface {
    email: string;
    username: string;
    password: string;
    admin: boolean;
    created: Date;
    updated: Date;

    constructor(
        email: string,
        username: string,
        password: string,
        admin: boolean,
        created: Date,
        updated: Date
    ) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.admin = admin;
        this.created = created;
        this.updated = updated;
    }

    auth(inputPassword: string, cb: any): any {  // add auth method
        bcrypt.compare(inputPassword, this.password, (err: any, isMatch: boolean) => {
            if (err) {
                console.log('bcrypt error: ', err);
                cb(false);
            } else {
                cb(isMatch);
            }
        });
    };
    // add method to return user object without plain-text password attached
    withoutPassword(inputUser: UserWithPassInterface): UserNoPassInterface {
        let user: UserWithPassInterface = inputUser;
        delete user.password;
        return user;
    };
};

export let uSchema: mongoose.Schema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
}).pre('save', function(next: any): void {
    if (this.isNew) {
        this.created = Date.now();
    }
    if (this.isNew || this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, salt);
        this.updated = Date.now();
        next();
    } else {
        next();
    }
});
uSchema.methods = {
    auth: User.prototype.auth,
    withoutPassword: User.prototype.withoutPassword
};

export interface UserDocument extends UserWithPassInterface, mongoose.Document { };
export let UserSchema: mongoose.Model<UserDocument> = mongoose.model<UserDocument>('User', uSchema);
