import { createTransport } from 'nodemailer';

// create reusable transporter object using the default SMTP transport
// see https://nodemailer.com/about/ for configuring to specific mail service
export const transporter: any = createTransport({
    service: 'gmail',
    auth: {
        user: 'your email',  // change these for production
        pass: 'your password'
    }
});

export interface EmailResInterface {
    r: object;
    bool: boolean;
};
// a typsescript class and constructor for creating email response
export class EmailResponse {
    r: object;
    bool: boolean;

    constructor(r: object, bool: boolean) {
        this.r = r;
        this.bool = bool;
    }
};
