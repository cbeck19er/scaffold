import { transporter, EmailResInterface, EmailResponse } from '../models/mailer';
import { UserNoPassInterface } from '../models/user';

let emRes: EmailResInterface;  // object to store email response
// create reusable transporter object using the default SMTP transport
// see https://nodemailer.com/about/ for configuring to specific mail service
// interface EmailResponseInt extends EmailResInterface { };
interface MailOptionsInt {
    from: string;
    to: string;
    subject: string;
    text: string;
    html: string;
};

export let mailIt = function mailer(u: UserNoPassInterface, returnMail: any) {  // todo:  MAILIT AND MAILER NEED TYPEDEFS <------
    let username: string = u.username;
    let email: string = u.email;
    // setup email data with unicode symbols
    let mailOptions: MailOptionsInt = {
        from: 'register@email.com' + '&gt;', // put an email address for sending out registration confirmations here
        to: email, // email address of the user who registered
        subject: 'Thank you for registering', // subject line
        text: 'Thank you for signing up for our service.  Your username is ' + username + '.',  // plain text body
        html: 'Hello, <br/> Thank you for registering for our service.  Your username is: <br/>'
        + username + '<br/>This message was intended for ' + email + '.' // html body
    };

    // verify connection configuration
    transporter.verify(function (e: any, success: boolean): void {
        if (e) {  // email not ready
            emRes = new EmailResponse(e, true);
            console.log('email transport verify error - ', e);
            returnMail(emRes);
        } else {  // good to go
            console.log('Email server is ready to take our messages');
            // send mail with defined transporter object
            transporter.sendMail(mailOptions, (e: any, info: any) => {
                // email not sent
                if (e) {
                    console.log('other email error - ', e);
                    emRes = new EmailResponse(e, true);
                    returnMail(emRes);
                } else {
                    // email sent
                    console.log('Message sent: ', info.messageId, info.response);
                    emRes = new EmailResponse(info, false);
                    returnMail(emRes);
                }
            });
        }
    });
};
