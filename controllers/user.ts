import { Request, Response } from 'express';
export let passport: any = require('passport');
let Strategy: any = require('passport-local').Strategy;
import * as jwt from 'jsonwebtoken';
let token: string = '';
import { Secret } from '../settings';
import { mailIt } from '../controllers/mailer';
import { EmailResInterface } from '../models/mailer';
import { ReqUserInterface } from '../middlewares/auth';
import { UserWithPassInterface, UserNoPassInterface, UserMethodsInterface, UserSchema, uSchema, UserDocument } from '../models/user';
interface UserWithPassInt extends UserWithPassInterface { };
interface UserNoPassInt extends UserNoPassInterface { };
interface UserMethodsInt extends UserMethodsInterface { };
interface ReqLogoutInterface extends Request {
    logout: any;  // todo: PROBABLY ADD USER TYPE HERE INSTEAD OF ANY
};
interface ErrStatusInterface {
    genError: {
        status: number
        message: string
    };
    userDupErr: {
        status: number
        message: string
    };
    userNoErr: {
        status: number
        message: string
    };
    loginErr: {
        status: number
        message: string
    };
    forbErr: {
        status: number
        message: string
    };
};
interface MessageStatusInterface {
    login: {
        status: number
        message: string
    };
    create: {
        status: number
        message: string
    };
    logout: {
        status: number
        message: string
    };
};
let errStatus: ErrStatusInterface = {
    genError: {
        status: 500,
        message: 'Server Error'
    },
    userDupErr: {
        status: 409,
        message: 'Please select a different username, that name is taken.'
    },
    userNoErr: {
        status: 403,
        message: 'Incorrect username, please try again.'
    },
    loginErr: {
        status: 403,
        message: 'Incorrect username or password, please try again.'
    },
    forbErr: {
        status: 403,
        message: 'Forbidden.  You do not have permission to access this area.'
    }
};
let messageStatus: MessageStatusInterface = {
    login: {
        status: 200,
        message: 'You have been logged-in successfully.  Token issued.'
    },
    create: {
        status: 200,
        message: 'You have successfully created a new account under the name '
    },
    logout: {
        status: 200,
        message: 'You have been logged-out successfully.'
    }
};

// passport methods
passport.serializeUser(function (user: UserWithPassInt, done: any): any {
    done(null, user);
});
passport.deserializeUser(function (user: UserWithPassInt, done: any): any {
    done(null, user);
});
passport.use(new Strategy((username: string, password: string, done: any): any => {
    UserSchema.findOne({
        username: username
    }, (err: any, currentUser: UserMethodsInterface) => {
        if (err) {
            console.log('database error: ', err);
            return done(errStatus.genError);
        }
        if (!currentUser) {  // username not found
            return done(null, false, errStatus.userNoErr);
        } else {
            // check passwordAttempt hash against stored hash
            currentUser.auth(password, (isCorrect: boolean) => {
                // if incorrect password
                if (isCorrect === false) {
                    return done(null, false, errStatus.loginErr);
                } else if (isCorrect === true) {
                    return done(null, currentUser, messageStatus.login);
                }
            });
        }
    });
}));

// logout current user (/users/logout)
export let logout: any = function (req: ReqLogoutInterface, res: Response): void {
    req.logout();
    token = '';
    res.render('login', {
        title: 'Login', warn: messageStatus.logout.message.toString()
    });
};

// login (post) current user (/users/login)
export let login: any = function (req: Request, res: Response, next: any): void {
    passport.authenticate('local', { session: false }, function (err: any, user: any, message: any): any {
        if (err) {  // server or database error
            console.log('error: ', err);
            res.status(errStatus.genError.status);
            return next(err);
        }
        // note:  user will be false if, for instance, client-side 'required' form checking fails for some reason;
        // i.e.form is submitted with no user
        if (!user || message.status !== 200) {
            res.status(errStatus.userNoErr.status)
            .render('login', {
                title: 'Login', warn: message.message.toString()
            });
        }
        if (user) {
            console.log('user from login function = ', user._doc);
            let userNoPass: UserNoPassInt = uSchema.methods.withoutPassword(user._doc);
            console.log('userNoPass from login function = ', userNoPass);
            token = jwt.sign({
                data: userNoPass
            }, Secret, { expiresIn: '1m' });
            res.cookie('token', token)
            .status(messageStatus.login.status)
            .render('success', {
                title: 'Success', message: message.message.toString()
            });
        };
    })(req, res, next);
};

let doMail: (user: UserNoPassInt) => void;
// create (post) new user (/users/create)
export let create: any = function (req: Request, res: Response): void {
    // check request body
    let name: string = req.body.username;
    console.log('req.body.username =', name);
    // check if username is taken
    UserSchema.findOne({
        username: req.body.username
    }, (err: any, result: UserDocument) => {
        if (err) {  // if error, do this
            console.log('mongo/mongoose error: ', err);
            res.status(errStatus.genError.status)
            .render('error', { title: 'Please try again', error: 'Error in submitting user.  ' + err.toString() });
        } else if (result !== null) {  // if name taken, do this
            res.status(errStatus.userDupErr.status)
            .render('create', { title: 'Create a User', message: errStatus.userDupErr.message });
        } else {  // no error, name not taken, save to db and route
            let newUser: UserDocument = new UserSchema(req.body);
            newUser.save((err: any, user: UserDocument) => {
                if (err) {
                    console.log('error: mongo/mongoose error- ', err);
                    res.status(errStatus.genError.status)
                    .render('error', { title: 'Error', error: err.toString() });
                } else {
                    // warn:  function delcarations not allowed inside blocks when in sctrict mode,
                    // move this mail function to a middleware or something
                    doMail = function (user: UserNoPassInt): void {  // send confirmation email
                        mailIt(user, function returnMail(response: EmailResInterface): void {
                            console.log('error status from doMail = ', response.bool);
                            console.log('error message from doMail = ', response.r);
                            if (response.bool === true) {
                                res.status(errStatus.genError.status)
                                .render('error', {
                                    title: 'Error',
                                    error: 'New user created as - ' + user.username
                                    + '.However, there was a problem sending the confirmation email.  ' + response.r.toString()
                                });
                                // todo:  ADD SOME METHOD TO RETRY SENDING CONFIRMATION EMAIL
                            } else {
                                res.status(messageStatus.create.status)
                                .render('success', {
                                    title: 'Success',
                                    message: messageStatus.create.message.toString() + user.username
                                    + '.  Confirmation email successfully sent to ' + user.email + '.'
                                });
                            }
                        });
                    };
                    doMail(user);
                }
            });
        }
    });
};

// check if logged-in user is same as user requested in url
export let loggedIn: any = function (req: ReqUserInterface, res: Response, next: any): void {
    let reqUser: any = req.params.reqUser;
    console.log('reqUser from route params =', reqUser);
    // check if username in params matches username from req.user
    if (reqUser !== req.user.username || !req.user) {
        res.status(errStatus.forbErr.status)
            .render('error', {
                title: 'Error',
                error: errStatus.forbErr.message
            });
    } else {
        res.render('username', {
            title: 'Welcome, ' + req.user.username,
            email: req.user.email,
            memberSince: 'Member since ' + req.user.created
        });
    }
};
