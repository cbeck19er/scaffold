// followed info found here for storing jwt in cookie:
// https://decembersoft.com/posts/authenticating-a-session-cookie-in-express-middleware-with-jsonwebtoken/
// storing tokens in cookie seems to be more secure:
// https://stormpath.com/blog/where-to-store-your-jwts-cookies-vs-html5-web-storage
import { Request, Response } from 'express';
import { Secret } from '../settings';
import * as jwt from 'jsonwebtoken';
// add type user to request interface
export interface ReqUserInterface extends Request {
    user: any;  // todo: PROBABLY ADD USER TYPEDEF HERE INSTEAD OF ANY
};
export let authorize: any = function (req: ReqUserInterface, res: Response, next: any): void {
    console.log('req from authRouter = ', req);
    // helper method to clear a token and invoke the next middleware
    function clearTokenAndNext(): void {
        res.clearCookie('token');
        req.user = {};
        next();
    }
    // read the cookie named 'token', if doesn't exist then return clear func
    let cookies: string[] = parseCookies(req);
    let token: string = cookies['token'];
    if (!token) {
        return clearTokenAndNext();
    } else {
        // check if token is valid
        jwt.verify(token, Secret, (err: Error, decodedToken: any) => {
            if (err) {  // if error returned, do this
                return clearTokenAndNext();
            }
            if (decodedToken.exp <= (Date.now() / 1000)) {  // check if token is expired
                return clearTokenAndNext();
            } else {
                req.user = decodedToken.data;
            }
        });
    }
    next();
};

// gets list of cookies so we can find ours
function parseCookies(request: ReqUserInterface): string[] {
    let list: string[] = [];
    let rc: string = request.headers.cookie;
    rc && rc.split(';').forEach(function (cookie: string): void {
        let parts: string[] = cookie.split('=');
        list[parts[0].trim()] = decodeURI(parts.slice(1).join('='));
    });

    return list;
};
