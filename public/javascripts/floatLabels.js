// thx zurb:  https://foundation.zurb.com/building-blocks/blocks/floated-label-wrapper.html
$(document).ready(function () {
    $(function () {
        var showClass = 'show';
        $('input').on('checkval', function () {
            var label = $(this).prev('label');
            if (this.value !== '') {
                label.addClass(showClass);
            } else {
                label.removeClass(showClass);
            }
        }).on('keyup', function () {
            $(this).trigger('checkval');
        });
    });
});
